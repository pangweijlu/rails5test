class TempController < ApplicationController
    
    def index1
   
    respond_to do |format|
      format.html # index.html.erb
      
    end
  end
    
    def docalc
  
      temp=params[:temp]
	  type=params[:type]
  
	  convertedTemp= Temp.ConvertTemp(temp,type)
      notice_string='docalc: temperature '+ params[:temp] +' in '+ \
      params[:type].to_s+ ' is '+ convertedTemp.to_s
	 
      respond_to  do |format|
          
		  format.html { redirect_to '/temp/index', notice: notice_string}		  
     
       end

  
    end
    
    def show
  
      temp=params[:temp]
	  type=params[:type]
  
	  convertedTemp=Temp.ConvertTemp(temp,type)
      notice_string='temperature '+ params[:temp] +' in '+ params[:type].to_s+ ' is '+ convertedTemp.to_s
	  
	
      respond_to  do |format|
          
		  format.html { redirect_to '/temp/index', notice: notice_string}		  
     
       end
  
  end
    
    private
  
     def ConvertTemp(temp,type)
	 
	    tempnum=temp.to_i
	    
		if type.eql?'celsius'
		
		  tt=tempnum*9/5+32
		
		else
		
		  tt=(tempnum-32)*5/9
		
		end
	 
	 
	 end
    
end
