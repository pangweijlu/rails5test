class CreateTemps < ActiveRecord::Migration[5.0]
  def change
    create_table :temps do |t|
      t.float :far
      t.float :cel

      t.timestamps
    end
  end
end
